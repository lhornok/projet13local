#!/usr/bin/env bash

kubectl config set-context dev --namespace=dev \
  --cluster=kubernetes \
  --user=kubernetes-admin

kubectl config set-context test --namespace=test \
  --cluster=kubernetes \
  --user=kubernetes-admin

kubectl config set-context prod --namespace=prod \
  --cluster=kubernetes \
  --user=kubernetes-admin

  kubectl config set-context gitlab --namespace=gitlab \
  --cluster=kubernetes \
  --user=kubernetes-admin